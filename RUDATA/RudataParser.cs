﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Efir.DataHub.Client;
using Efir.DataHub.Entities;
using Efir.DataHub.Entities.ArchiveService.Contracts;
using Efir.DataHub.Entities.InfoService.Contracts;
using Efir.DataHub.Services;

namespace RUDATA
{
    /// <summary>
    /// Данный класс собирает в себе все необходимое для получения данных из
    /// RUDATA
    /// </summary>
    class RudataParser
    {
        /// <summary>
        /// метод использует функцию info/Instruments  и возвращает частично заполненную
        /// структуру curs 
        /// </summary>
        public static List<Curs> getIdCurses()
        {
            List<Curs> curslist = new List<Curs>();
            Efir.DataHub.Services.InfoServiceClient some = new Efir.DataHub.Services.InfoServiceClient(RudataConnection.clientConnectionSettings);
            some.Token = RudataConnection.getToken();
            var result = some.GetInstrumentList(new GetInstrumentListRequest() { filter = "EXCH=33 AND VISIBLE=\u0022Видимый\u0022" }, CancellationToken.None);
            DataTable table = result.Result.AsDataTable();
            //
            for (int i = 0; i < table.Columns.Count; i++)
            {
                Console.WriteLine(table.Columns[i]);
            }
            for (int i = 0; i < table.Rows.Count; i++)
            {
                curslist.Add(new Curs());
                curslist[i].rudataID = Int32.Parse(table.Rows[i].ItemArray[0].ToString()); // table.Rows[i].ItemArray[0];//ID курса валют в RUDATA
                                                                                           //    Console.WriteLine(table.Rows[i].ItemArray[0]);
                curslist[i].from = table.Rows[i].ItemArray[1].ToString()
                    .Remove(table.Rows[i].ItemArray[1].ToString().IndexOf("/"));//парсинг строки результат- валюта FROM
                if (table.Rows[i].ItemArray[1].ToString().IndexOf("CBR") != 0)
                {
                    string buf = table.Rows[i].ItemArray[1].ToString().Remove(table.Rows[i].ItemArray[1].ToString().IndexOf("."));
                    curslist[i].to = buf.Substring(buf.IndexOf("/") + 1);
                    //парсинг мантиссы 
                    if (table.Rows[i].ItemArray[1].ToString().IndexOf("1") != -1)
                    {
                        buf = table.Rows[i].ItemArray[1].ToString()
                            .Substring(table.Rows[i].ItemArray[1].ToString().IndexOf("1"));
                        curslist[i].mantissa = Int32.Parse(buf.Remove(buf.IndexOf(".")));
                    }
                    else
                    {
                        curslist[i].mantissa = 1;
                    }
                    curslist[i].shortSourse = "CBR";
                    curslist[i].sourse = table.Rows[i].ItemArray[3].ToString();
                }
                else
                {// Обозначает что данные взяте не из CBR или не указан источник получения данных,
                 // в настоящее время в данный блок никогда не должен выполняться
                    throw new System.Exception();
                }
            }

            return curslist;
        }

        public static void EndOfDayFunc(List<Curs> curslist, string date)
        {
            Efir.DataHub.Services.ArchiveServiceClient client =
                new ArchiveServiceClient(RudataConnection.clientConnectionSettings);
            client.Token = RudataConnection.getToken();
            GetEndOfDayRequest inputData = new GetEndOfDayRequest();

            inputData.date = "2018-03-06T00:00:00.0000000Z";
            //inputData.date = date.ToString().Replace(' ', 'T');
            inputData.fields = "TIME,LAST";
            for (int i = 0; i < curslist.Count; i++)
            {
                inputData.issId = curslist[i].rudataID;
                var result = client.GetEndOfDay(inputData, CancellationToken.None);
                DataTable table = result.Result.AsDataTable();
                for (int j = 0; j < table.Rows.Count; j++)
                {
                    //if (decimal.Zero.Equals(curslist[i].rate)|| compareTwoStringDates(table.Rows[j].ItemArray[0].ToString(),fromDateToString(curslist[i].datetime)))
                    if (compareTwoStringDates(table.Rows[j].ItemArray[0].ToString(), fromDateToString(curslist[i].datetime)))
                    {
                        curslist[i].rate = decimal.Parse(table.Rows[j].ItemArray[1].ToString());
                    }
                }

            }
        }

        public static string fromDateToString(DateTime date)
        {
            return (date.ToString().Replace(' ', 'T').Replace('.', '-')) + ".0000000Z";
        }

        public static bool compareTwoStringDates(string first, string second)
        {
            string buffirst = first;
            string bufsecond = second;
            string firstyear = first.Remove(first.IndexOf("-"));
            string secondyear = second.Remove(second.IndexOf("-"));
            //
            Console.WriteLine("firstyear=" + firstyear);
            Console.WriteLine("secondyear=" + secondyear);
            //сравниваем год
            if (Int32.Parse(firstyear) > Int32.Parse(secondyear))
            {
                return true;
            }
            else
            {
                buffirst = buffirst.Substring(buffirst.IndexOf("-") + 1);
                string firstmonth = buffirst.Remove(buffirst.IndexOf("-"));
                bufsecond = bufsecond.Substring(bufsecond.IndexOf("-") + 1);
                string secondmonth = bufsecond.Remove(bufsecond.IndexOf("-"));
                //
                Console.WriteLine("firstmonth=" + firstmonth);
                Console.WriteLine("secondmonth=" + secondmonth);
                //
                if (Int32.Parse(firstmonth) > Int32.Parse(secondmonth))
                {
                    return true;
                }
                else
                {
                    buffirst = buffirst.Substring(buffirst.IndexOf("-") + 1);
                    string firstday = buffirst.Remove(buffirst.IndexOf("T"));
                    bufsecond = bufsecond.Substring(bufsecond.IndexOf("-") + 1);
                    string secondday = bufsecond.Remove(bufsecond.IndexOf("T"));
                    Console.WriteLine("firstday=" + firstday);
                    Console.WriteLine("seconddat=" + secondday);
                    if (Int32.Parse(firstday) > Int32.Parse(secondday))
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }

            }
        }

    }

















}
