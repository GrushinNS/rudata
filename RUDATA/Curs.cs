﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RUDATA
{
    

    class Curs
    {
        public int rudataID;
        public string from;
        public string to;
        public decimal rate=decimal.Zero;
        /// <summary>
        /// Источник получения курса валюты,как правило CBR(центробанк)
        /// </summary>
        public string sourse;
        public string shortSourse;
        public System.DateTime datetime;
        public int mantissa;
       // с помощью какой функции получены данные (служит для ознакомления )
        public string sourseFunc;
        
        public Curs()
        {

        }

        public  void printCurs()
        {
            Console.WriteLine("rudataID: "+this.rudataID);
            Console.WriteLine("FROM: "+this.from);
            Console.WriteLine("TO: "+this.to);
            Console.WriteLine("Rate: "+this.rate);
            Console.WriteLine("mantissa: "+this.mantissa);
            Console.WriteLine("SOURSE: "+this.sourse);
            Console.WriteLine("SHORTSOURSE:"+ this.shortSourse);
            Console.WriteLine("Date:="+this.datetime);
            Console.WriteLine("sourseFunc: "+this.sourseFunc);
            Console.WriteLine("-----------------------------------------------------------------------------");

        }

        public static void printCursList(List<Curs> list)
        {
            for (int i = 0; i < list.Count; i++)
            {
                Console.WriteLine("NRECORD="+i);
                list[i].printCurs();
            }
            Console.WriteLine("nCurses="+(list.Count+1));
        }
    }
}
