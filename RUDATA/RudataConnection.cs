﻿

using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Efir.DataHub.Client;
using Efir.DataHub.Entities.AccountService.Contracts;
using Efir.DataHub.Services;


namespace RUDATA
{
    /// <summary>
    /// Класс для подключения к rudata
    /// </summary>
    class RudataConnection
    {


        private static string SecurityToken;        
        public static Uri rudataUri = new Uri("https://new-datahub.efir-net.ru/hub.axd");

        private const string LOGIN = "ingos-test-web2";
        private const string PASSWORD = "ApYbdXk";
        private static LoginRequest loginrequest = new LoginRequest()
        {
            login = LOGIN,
            password = PASSWORD,
        };
        public static ClientConnectionSettings clientConnectionSettings = new ClientConnectionSettings()
        {

            BaseUri = rudataUri,
            Format = ClientMessageFormat.Json,
        };

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public static string getToken()
        {
            if (String.IsNullOrEmpty(SecurityToken) == false)
            {
                return SecurityToken;
            }
            else
            {
                try
                {

                   
                    using (var accountClient = new AccountServiceClient(clientConnectionSettings))
                    {
                        using (var result = accountClient.LoginAsync(loginrequest, CancellationToken.None))
                        {
                            SecurityToken = result.Result.Result.Token;
                            Console.WriteLine(SecurityToken);
                           
                        }

                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    // MessageBox.Show("Error while login:" + ex);
                  //  return ;
                }
                return SecurityToken;
            }

        }
    }
}
